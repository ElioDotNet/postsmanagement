// Usiamo Express e creiamo la app
const express = require("express");
// Viene usata per fare il parsing del body nelle requests
const bodyParser = require("body-parser");
// Utilizzo di Express
const app = express();
// Utilizzo di mongoose per connessione a Mongo(node-angular viene creato appena salviamo dei dati)
const mongoose = require("mongoose");
// Utilizzo del Rooting per i Posts e gli Users
const postsRoutes = require("./routes/posts");
const userRoutes = require("./routes/user");

// Serve per comporre il Path nei vari sistemi operativi
const path = require("path");

mongoose
  .connect(
    "mongodb+srv://elio:" +
    // Password per accedere a Mongo Atlas, disponibile nelle variabili di ambiente di NodeJs
      process.env.MONGO_ATLAS_PW +
      "@cluster0-afsqd.mongodb.net/node-angular?retryWrites=true"
  )
  .then(() => {
    console.log("Connected to Mongo");
  })
  .catch(error => {
    console.log(error); // 'Error to connect to Mongo'
  });

app.use(bodyParser.json()); // trasforma in json il body nelle requests
app.use(bodyParser.urlencoded({ extended: false })); // decodifica degli url
// Qualsiasi richiesta di '/images' autorizza l'accesso al path specifico
app.use("/images", express.static(path.join("backend/images")));

// Inseriamo un Middlelware per gestire Il CORS(Cross-Origin Resources Sharing)
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*"); // per il Browser: non importa da quale domain arriva la richiesta, autorizza
  res.setHeader(
    // sono autorizzati solo questi tipi di Header
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, PUT, DELETE, OPTIONS" // metodi autorizzati(OPTIONS va sempre messo, Angular lo aggiunge di default)
  );
  next();
});

// Tutte le richieste che iniziano con "/api/posts" saranno reindirizzate in '/routes/posts'
app.use("/api/posts", postsRoutes);
// Tutte le richieste che iniziano con "/api/user" saranno reindirizzate in userRoutes './routes/user'
app.use("/api/user", userRoutes);

// Esportiamo Espress
module.exports = app;
