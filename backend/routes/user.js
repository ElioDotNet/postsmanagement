const express = require("express");

const router = express.Router(); // router fornito da Express

const UserController = require("../controllers/user"); // importiamo le funzioni dello user

router.post("/signup", UserController.createUser);

router.post("/login", UserController.userLogin);

module.exports = router;
