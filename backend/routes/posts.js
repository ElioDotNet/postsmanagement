const express = require("express");
// Router fornito da Express
const router = express.Router();
// Verifica Token
const checkAuth = require("../middleware/check-auth");
const extractFile = require("../middleware/file");

const PostController = require("../controllers/posts"); // importiamo le funzioni del post

// POST
// Multer si aspetta una immagine singola nella request con la proprietà 'image'
router.post(
  "",
  checkAuth, // verifica del token, prima di eseguire multer
  extractFile, // la parte del file è stata spostata in un file a parte
  PostController.createPost);

// PATCH
router.patch(
  "/:id",
  checkAuth, // verifica del token, prima di eseguire multer
  extractFile, // la parte del file è stata spostata in un file a parte
  PostController.updatePost);

// GET(legge tutti i posts)
router.get("", PostController.getPosts);

// GET(legge un post)
router.get("/:id", PostController.getPost);

// DELETE
router.delete("/:id", checkAuth, PostController.deletePost);

module.exports = router;
