const jwt = require("jsonwebtoken");
// Esportiamo una funzione per controllare la validità del token
module.exports = (req, res, next) => {
  try {
    // Inseriamo l'autorization nel header e il token dopo lo spazio(potrebbe generare errore)
    const token = req.headers.authorization.split(" ")[1]; // per convenzione il token davanti ha la parola Bearer o spazio
    // Chiave segreta per decodificare il token, disponibile nelle variabili di ambiente di NodeJs
    const decodedToken = jwt.verify(token, process.env.JWT_KEY);
    // Express.js ci consente di aggiungere dei dati nella request
    req.userData = { email: decodedToken.email, userId: decodedToken.userId };
    next(); // verifica ok, si prosegue
  } catch (error) { // errore decodifica token
    res.status(401).json({ message: "you are not authenticated!" });
  }
};
