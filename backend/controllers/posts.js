// IN QUESTO MODULO SPOSTIAMO LE FUNZIONALITA' DEL route/post
const Post = require("../models/post");

exports.createPost = (req, res, next) => {
  const url = req.protocol + "://" + req.get("host");
  const post = new Post({
    title: req.body.title,
    content: req.body.content,
    imagePath: url + "/images/" + req.file.filename, // viene restituito da multer
    creator: req.userData.userId // restituito da checkAuth
  });
  // Salva i dati in Mongo(la collection si chiamerà con il nome plurale del modulo cioè posts)
  post.save().then(createdPost => {
    res.status(201).json({
      message: "Post added successfully",
      post: {
        ...createdPost,
        id: createdPost._id
      }
    });
  })
  .catch(() => {
    res.status(500).json({
      message: "Creating a post failed!"
    });
  });
};

exports.updatePost = (req, res, next) => {
  let imagePath = req.body.imagePath;
  if (req.file) {
    const url = req.protocol + "://" + req.get("host");
    imagePath = url + "/images/" + req.file.filename;
  }
  const post = new Post({
    _id: req.body.id,
    title: req.body.title,
    content: req.body.content,
    imagePath: imagePath,
    creator: req.userData.userId
  });
  Post.updateOne( // metodo di Mongoose: controlla le due condizioni prima di fare update
    {_id: req.params.id, creator: req.userData.userId},
    post
  ).then(result => {
    if (result.n > 0) { // solo chi ha creato il post lo può modificare
      res.status(200).json({message: "Update successful!"});
    } else {
      res.status(401).json({message: "Not authorized!"});
    }
  })
  .catch(() => {
    res.status(500).json({
      message: "Couldn't updated post!"
    })
  });
};

exports.getPosts = (req, res, next) => {
  // Impostiamo i parametri inseriti in query string
  const pageSize = +req.query.pageSize;
  const currentPage = +req.query.page;
  const postQuery = Post.find(); // il find viene eseguito solo quando si chiama il then
  let fetchedPosts;
  if (pageSize && currentPage) {
    // se abbiamo impostato dei parametri
    postQuery
      .skip(pageSize * (currentPage - 1)) // occorre skippare questi elementi
      .limit(pageSize); // con un limite legato al page size
  }
  postQuery
    .then(documents => {
      fetchedPosts = documents;
      return Post.count(); // totale dei posts
    })
    // posso concatenare più then
    // Leggiamo tutti i dati da Mongo
    .then(count => {
      return res.status(200).json({
        // restituisce una response(positiva) in formato Json
        message: "Posts fetched successfully",
        posts: fetchedPosts,
        maxPosts: count // numero di posts presenti
      });
    })
    .catch(() => {
      res.status(500).json({
        message: "Fetching posts failed!"
      });
    });
};

exports.getPost = (req, res, next) => {
  Post.findById(req.params.id).then(post => {
    if (post) {
      res.status(200).json(post);
    } else {
      res.status(404).json({message: "Post not found!"});
    }
  })
  .catch(() => {
    res.status(500).json({
      message: "Fetching post failed!"
    });
  });
};

exports.deletePost = (req, res, next) => {
  // Anche per la delete occorre essere autorizzati
  // Cancellazione da Mongo usando deleteOne di Mongoose controllando le condizioni
  Post.deleteOne({_id: req.params.id, creator: req.userData.userId}).then(
    result => {
      if (result.n > 0) {
        // solo chi ha creato il post lo può cancellare
        res.status(200).json({message: "Delete successful!"});
      } else {
        res.status(401).json({message: "Not authorized!"});
      }
    }
  )
  .catch(() => {
    res.status(500).json({
      message: "Deleting post failed!"
    });
  });
};
