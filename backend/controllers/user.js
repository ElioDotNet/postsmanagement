// IN QUESTO MODULO SPOSTIAMO LE FUNZIONALITA' DEL route/user
// bcryptjs serve per cryptare la password
const bcrypt = require("bcryptjs");
// jwt per generare il token
const jwt = require("jsonwebtoken");
const User = require("../models/user");

exports.createUser = (req, res, next) => {
  bcrypt
    .hash(req.body.password, 10)
    // Quando eseguo la promise avrò la password criptata
    .then(hash => {
      const user = new User({
        email: req.body.email,
        password: hash
      });
      // Salviamo lo user
      user
        .save()
        .then(result => {
          res.status(201).json({
            message: "User created",
            result: result
          });
        })
        .catch(() => {
          res.status(500).json({
            message: "Invalid authentication credentials!"
          });
        });
    });
  };

  exports.userLogin = (req, res, next) => {
    let fetchedUser;
    // Verifica se esiste la mail
    User.findOne({email: req.body.email}).then(user => {
      if (!user) {
        return res.status(401).json({
          message: "Auth failed"
        });
      }
      fetchedUser = user;
      // Verifica password nel db, con quella inserita
      return bcrypt.compare(req.body.password, user.password);
    })
      .then(result => {
        if (!result) { // le password non coincidono
          return res.status(401).json({
            message: "Auth failed"
          });
        }
        // Creazione del token dopo che lo user è stato identificato
        const token = jwt.sign({email: fetchedUser.email, userId: fetchedUser._id},
          // Chiave segreta per decodificare il token, disponibile nelle variabili di ambiente di NodeJs
          process.env.JWT_KEY,
          {expiresIn: "1h"} // scadenza token
        );
        // Ritorniamo il token e quando scade
        res.status(200).json({
          token: token,
          expiresIn: 3600,
          // Lo userId si potrebbe anche decodificando il tk, ma è meglio restituirlo
          userId: fetchedUser._id
        });
      })
      .catch(() => { // errore verifica password
        return res.status(401).json({
          message: "Invalid authentication credentials!"
        });
      })
  };
