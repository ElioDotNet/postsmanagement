const mongoose = require('mongoose');
// Utilizziamo utility di Mongoose per avere un campo univoco
const uniqueValidator = require('mongoose-unique-validator');

// Definizione dello schema dati degli users con Mongoose
const userSchema = mongoose.Schema({
  // unique NON è un validatore serve solo a Mongoose per ottimizzare il salvataggio dei dati
  email: { type: String, required: true, unique: true},
  password: { type: String, required: true}
});
// Aggiungiungendo il plug-in si avrà un errore se non si utilizza un valore univoco
userSchema.plugin(uniqueValidator);

// Definizione del Model(Il nome deve iniziare con lettera maiuscola)
module.exports = mongoose.model('User', userSchema);
