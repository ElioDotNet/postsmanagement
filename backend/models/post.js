const mongoose = require('mongoose');

// Definizione dello schema dati dei Posts con Mongoose
const postSchema = mongoose.Schema({
  title: {type: String, require: true},
  content: {type: String, require: true},
  imagePath: {type: String, require: true},
  // User che ha creato il post che fa riferimento allo schema "User"
  creator: {type: mongoose.Schema.Types.Object, ref: "User", require: true}
});

// Definizione del Model(Il nome deve iniziare con lettera maiuscola)
module.exports = mongoose.model('Post', postSchema);
