// Importiamo la App
const app = require("./app");
const debug = require("debug")("node-angular"); // il nome node-angular è di fantasia
const http = require("http");
// Verifichiamo che la porta usata sia un numero valido
const normalizePort = val => {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
};
// Funzione per gestire gli errori
const onError = error => {
  if (error.syscall !== "listen") {
    throw error;
  }
  const bind = typeof port === "string" ? "pipe " + port : "port " + port;
  switch (error.code) {
    case "EACCES":
      console.error(bind + " requires elevated privileges");
      process.exit(1);
      break;
    case "EADDRINUSE":
      console.error(bind + " is already in use");
      process.exit(1);
      break;
    default:
      throw error;
  }
};
// Leggiamo le richieste in arrivo
const onListening = () => {
  const bind = typeof port === "string" ? "pipe " + port : "port " + port;
  debug("Listening on " + bind);
};

// Usiamo la porta predefinita e se non esiste usiamo la 3000
const port = normalizePort(process.env.PORT || "3000");
// Settiamo i parametri per la App
app.set("port", port);

const server = http.createServer(app);
// Registriamo i le due funzioni
server.on("error", onError);
server.on("listening", onListening);
// Siamo in attesa nella porta
server.listen(port);
