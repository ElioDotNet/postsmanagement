import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { AuthService } from "./auth.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor{
  constructor(private authService: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authToken = this.authService.getToken();
    const authRequest = req.clone({ // cloniamo la request per non manipolarla
      headers: req.headers.set("Authorization", "Bearer " + authToken) // sovrascriamo l'header con authorization(usato nel backend)
    });
    return next.handle(authRequest); // continua l'esecuzione dopo l'interceptor, passando l'autorizzazione
  }
}
