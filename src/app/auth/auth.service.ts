import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';

import { AuthData } from './auth-data.model';

import { environment } from '../../environments/environment';

const BACKEND_URL = environment.apiUrl + '/user/';

// Viene creata una sola instanza del service per tutta l'applicazione
@Injectable({providedIn: 'root'})

export class AuthService {
  private isAuthenticated = false;
  private token: string;
  private userId: string;
  private tokenTimer: any;
  // Viene pushato nei component interessati, per indicare le lo user è autenticato o meno
  private authStatusListener = new Subject<boolean>();

  constructor(private http: HttpClient,
              private router: Router) {}

  getToken() {
    return this.token;
  }

  getUserId() {
    return this.userId;
  }

  getIsAuth() {
    return this.isAuthenticated;
  }

  getAuthStatusListener() {
    return this.authStatusListener.asObservable();
  }

  createUser(email: string, password: string) {
    const authData: AuthData = {email: email, password: password};
    this.http.post(BACKEND_URL + 'signup', authData)
      .subscribe(() => {
      this.router.navigate(['/']);
    }, () => {
      this.authStatusListener.next(false); // passiamo false per nascondere lo spinner
    });
  }

  autoAuthUser() {
    const authInfomation = this.getAuthData();
    if (!authInfomation) {
      return;
    }
    const now = new Date();
    const expireIn = authInfomation.expirationData.getTime() - now.getTime();
    if (expireIn) {
      this.token = authInfomation.token;
      this.isAuthenticated = true;
      this.userId = authInfomation.userId;
      this.setAuthTimer(expireIn / 1000);
      this.authStatusListener.next(true); // user autenticato
    }
  }

  login(email: string, password: string) {
    console.log(BACKEND_URL);
    const authData: AuthData = {email: email, password: password};
    this.http
      .post<{ token: string, expiresIn: number, userId: string }>(
        BACKEND_URL + 'login',
        authData
      )
      .subscribe(response => {
        const token = response.token;
        this.token = token;
        if (this.token) {
          // lo user è autenticato solo se c'è un token valido
          const expiresInDuration = response.expiresIn; // scadenza token
          this.setAuthTimer(expiresInDuration);
          this.isAuthenticated = true;
          this.userId = response.userId;
          this.authStatusListener.next(true); // si è autenticati
          const now = new Date();
          const expirationDate = new Date(
            now.getTime() + expiresInDuration * 1000
          );
          this.saveAuthData(token, expirationDate, this.userId);
          this.router.navigate(['/']); // reinderizzo alla home
        }
      }, () => {
        this.authStatusListener.next(false);
      });
  }

  logout() {
    // Occorre resettare i valori di login
    this.token = null;
    this.isAuthenticated = false;
    this.userId = null;
    this.authStatusListener.next(false); // non si è autenticati
    clearTimeout(this.tokenTimer); // azzeriamo il timer quando viene fatto il logout
    this.clearAuthData();
    this.router.navigate(['/']); // reinderizzo alla home
  }

  // Impostamo il timer
  private setAuthTimer(duration: number) {
    console.log('Setting timer: ', duration);
    this.tokenTimer = setTimeout(() => {
      this.logout(); // quando scade il token, occore fare il logout
    }, duration * 1000);
  }

  // Salviamo i dati nel localStorage
  private saveAuthData(token: string, expirationDate: Date, userId: string) {
    localStorage.setItem('token', token);
    localStorage.setItem('expiration', expirationDate.toISOString()); // in formato iso
    localStorage.setItem('userId', userId);
  }

  // Eliminiamo i dati dallo localStorage
  private clearAuthData() {
    localStorage.removeItem('token');
    localStorage.removeItem('expiration');
    localStorage.removeItem('userId');
  }

  // Leggiamo i dati dal localStorage
  private getAuthData() {
    const token = localStorage.getItem('token');
    const expirationData = localStorage.getItem('expiration');
    const userId = localStorage.getItem('userId');
    if (!token || !expirationData) {
      return;
    }
    return {
      token,
      expirationData: new Date(expirationData),
      userId
    };
  }
}
