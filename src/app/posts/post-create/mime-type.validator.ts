import { AbstractControl } from '@angular/forms';
import { Observable, Observer, of } from 'rxjs';

export const mimeType = (
  control: AbstractControl
): Promise<{ [key: string]: any }> | Observable<{ [key: string]: any }> => {
  if (typeof(control.value) === 'string') { // se è di tipo string è valido
    return of(null);
  }
  const file = control.value as File;
  const fileReader = new FileReader();
  const frObs = Observable.create( // si crea un Observable
    (observer: Observer<{ [key: string]: any }>) => {
      fileReader.addEventListener('loadend', () => {
        // Non sapendo di che tipo è l'allegato usiamo Uint8Array, per conoscerne il contenuto
        const arr = new Uint8Array(fileReader.result as ArrayBuffer).subarray(0, 4); // subarray(0, 4) prende il file
        let header = '';
        let isValid = false;
        for (let i = 0; i < arr.length; i++) {
          header += arr[i].toString(16);
        }
        switch (header) { // rappresentano i tipi di files
          case '89504e47':
            isValid = true;
            break;
          case 'ffd8ffe0':
          case 'ffd8ffe1':
          case 'ffd8ffe2':
          case 'ffd8ffe3':
          case 'ffd8ffe8':
            isValid = true;
            break;
          default:
            isValid = false;
            break;
        }
        // Viene restituito un observable
        if (isValid) {
          observer.next(null); // il file è valido, viene ritornato null
        } else {
          observer.next({ invalidMimeType: true }); // il file NON è valido, si restituisce un oggetto
        }
        observer.complete();
      });
      fileReader.readAsArrayBuffer(file); // leggiamo il file come array buffer
    }
  );
  return frObs;
};
