import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Subscription } from 'rxjs';

import { PostsService } from '../posts.service';
import { Post } from '../post.model';
import { mimeType } from './mime-type.validator';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})
export class PostCreateComponent implements OnInit, OnDestroy {
  post: Post;
  isLoading = false; // viene usata per lo spinner
  form: FormGroup; // viene usata per il reactive approach
  imagePreview: string;
  private mode = 'create';
  private postId: string;
  private authStatutsSub: Subscription;

  constructor(
    public postsService: PostsService,
    public route: ActivatedRoute,
    private authService: AuthService
  ) {}

  ngOnDestroy() {
    this.authStatutsSub.unsubscribe();
  }

  ngOnInit() {
    this.authStatutsSub = this.authService
      .getAuthStatusListener()
      .subscribe(() => {
        this.isLoading = false;
    });
    // Configurazione del form con dei valori iniziali a null
    this.form = new FormGroup({
      title: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(3)]
      }),
      content: new FormControl(null, { validators: [Validators.required] }),
      image: new FormControl(null, {
        validators: [Validators.required],
        asyncValidators: [mimeType] // validazione del file di upload
      })
    });
    // Viene eseguito ogni volta cambia il parametro
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('postId')) {
        this.mode = 'edit';
        this.postId = paramMap.get('postId');
        this.isLoading = true; // rendo visibile lo spinner
        this.postsService.getPost(this.postId).subscribe(postData => {
          this.isLoading = false;
          this.post = {
            id: postData._id,
            title: postData.title,
            content: postData.content,
            imagePath: postData.imagePath,
            creator: postData.creator
          };
          // Inizializziamo il Form con il post
          this.form.setValue({
            title: this.post.title,
            content: this.post.content,
            image: this.post.imagePath
          });
        });
      } else {
        this.mode = 'create';
        this.postId = null;
      }
    });
  }

  onImagePicked(event: Event) {
    // Impostiamo un singolo elemento del form con patchValue
    const file = (event.target as HTMLInputElement).files[0];
    this.form.patchValue({ image: file }); // il file vine considerato come un oggetto
    this.form.get('image').updateValueAndValidity(); // aggiorna il valore e verificane i validators
    const reader = new FileReader(); // per creare un url per l'immagine
    reader.onload = () => {
      this.imagePreview = reader.result as string; // viene eseguito quando vine letto il file
    };
    reader.readAsDataURL(file);
  }

  onSavePost() {
    if (this.form.invalid) {
      return;
    }
    this.isLoading = true;
    if (this.mode === 'create') {
      this.postsService.addPost(
        this.form.value.title,
        this.form.value.content,
        this.form.value.image);
    } else {
      this.postsService.updatePost(
        this.postId,
        this.form.value.title,
        this.form.value.content,
        this.form.value.image
      );
    }
    this.form.reset(); // pulisce i dati del form
  }
}
