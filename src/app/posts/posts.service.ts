import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';

import { Post } from './post.model';
import { environment } from '../../environments/environment';

const BACKEND_URL = environment.apiUrl + '/posts/';

@Injectable({providedIn: 'root'}) // viene creata una sola instanza del service per tutta l'applicazione
export class PostsService {
  private posts: Post[] = [];
  private postsUpdated = new Subject<{ posts: Post[]; postCount: number }>();

  constructor(private http: HttpClient, private router: Router) {}

  getPosts(postsPerPage: number, currentPage: number) {
    // Definisco i parametri passati al backend in query string
    const queryParams = `?pageSize=${ postsPerPage }&page=${ currentPage }`;
    this.http
      .get<{ message: string; posts: any; maxPosts: number }>(BACKEND_URL + queryParams)
      /* Utlizzando l'operatore map modifichiamo la risposta per
        ottenere l'id corrento da Mongo */
      .pipe(
        map(postData => {
          return {
            posts: postData.posts.map(post => {
              return {
                title: post.title,
                content: post.content,
                id: post._id,
                imagePath: post.imagePath,
                creator: post.creator
              };
            }),
            maxPosts: postData.maxPosts
          };
        })
      )
      .subscribe(transformedPostData => {
        this.posts = transformedPostData.posts;
        this.postsUpdated.next({
          posts: [...this.posts],
          postCount: transformedPostData.maxPosts
        });
      });
  }

  getPostUpdateListener() {
    return this.postsUpdated.asObservable(); // restituisce un observable
  }

  getPost(id: string) {
    return this.http.get<{
      _id: string,
      title: string,
      content: string,
      imagePath: string,
      creator: string }>(
        BACKEND_URL + id
    );
  }

  addPost(title: string, content: string, image: File) {
    // Dovendo aggiungere una immagine occore un FormData, non un Json
    const postData = new FormData();
    postData.append('title', title);
    postData.append('content', content);
    postData.append('image', image, title); // inserisco il titolo dell'immagine
    this.http
      .post<{ message: string; post: Post }>(
        BACKEND_URL, // chiamata al backend
        postData
      )
      .subscribe(responseData => {
        this.router.navigate(['/']); // torniamo alla pagina precedente
      });
  }

  updatePost(id: string, title: string, content: string, image: File | string) {
    let postData: any;
    if (typeof image === 'object') { // tipo File
      postData.append('id', id);
      postData.append('title', title);
      postData.append('content', content);
      postData.append('image', image, title);
    } else {
      postData = {
        id,
        title,
        content,
        imagePath: image,
        creator: null
      };
    }
    this.http
      .patch(BACKEND_URL + id, postData)
      .subscribe(() => {
        this.router.navigate(['/']); // torniamo alla pagina precedente
      });
  }

  deletePost(postId: string) {
    return this.http
      .delete(BACKEND_URL + postId);
  }
}
